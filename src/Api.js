import express from 'express';
import morgan from 'morgan';
import bodyparser from 'body-parser';

export default class Api {
  // annotate with the express $Application type
  express: express$Application;
  
  // create the express instance, attach app-level middleware, attach routers
  constructor(){
      this.express = express();
      this.middleware();
      this.routes();
  }
  
  // register middlewares
  middleware(): void {
      this.express.use(morgan('dev'));
      this.express.use(bodyparser.json());
      this.express.use(bodyparser.urlencoded({extended: false}));
  }

  // connect resource routers
  routes(): void {
    this.express.use((req: $Request, res: $Response) => {
      res.json({ message: 'Hello Flow!' });
    });
  }

}