# Node Api Server using Flow Type (type checking)




$ For development install packages using:

```bash
   $ npm install
   ```
$ For production install packages using:

```bash
   $ npm install --only=prod
   ```
$ Install gulp-cli globally if not already installed

```bash
   $ npm install -g gulp-cli
   ```
$ Install flow-typed globally

```bash
   $ npm install -g flow-typed
   ```
$ To install the libdefs for the packages (do this whenever a new npm library
  is installed)

```bash
   $ flow-typed install
   ```
   
   
